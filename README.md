# README #

This README  documents necessary steps  to get the application up and running.

* Version : 1

# Harry Potter Android #
This is an an Android App that queries data to Potter API to track and show infomation about Harry Poter movie.
In this application, the user is able:

* To View a list of houses and see all the characters within that house as well as extra information about that house
* View list All Students and obtain information about each student when clicked on a student.
* View list All Characters and obtain information about each charater when clicked on a character.
* View list All Spells and information about each Spell.

# Architecture # 

### The app is developed using: ###

* Model-View-ViewModel (MVVM) architecture
![MVVM Architecture](/images/Archetecture.jpg)
* Single Activity Model
* Navigation Component.
* Dagger2 Dependency Injection.
![Dependency Injecttion Mapping](/images/DaggerDependencyInjectionMapping.jpg)

# ScreenShots #
![List Of Houses](/images/houses_list.jpg)
![List Of Charaters In a House](/images/house_characters.jpg)
![List Of All Students](/images/students_list.jpg)
![Student Details](/images/student_info.jpg)
![Character Details](/images/character_info.jpg)
![List Of All the Spells](/images/spells_list.jpg)
## Who do I talk to? ##
* Repo owner or admin : Felly
* Cellphone: +27 65 715 9671
* Email : fellymuepu@outlook.com