package com.felly.harrypotter.core.utils;

import android.widget.ImageView;

import com.felly.harrypotter.R;
import com.squareup.picasso.Picasso;

public class Constants {
    public static final String BASE_URL = "https://www.potterapi.com/v1/";
    public static final String API_KEY = "$2a$10$1JEnmtEF417yBaFZcr51qukRjaKv8d5toEG5DKP/IUZWIVwfsaF7y";
    public static final String IMAGE_URL = "http://hp-api.herokuapp.com/images/";
    public static final String ERROR = "error";

    public static final String HOUSE_EXTRA = "house";
    public static final String STUDENTS_EXTRA = "student";
    public static final String CHARACTERS_EXTRA = "characters";
    public static final String SPELLS_EXTRA = "spells";

    public static int getHouseIncon(String houseName){
        int resource = 0;
        switch (houseName){
            case "Gryffindor":
                resource = R.drawable.gryffindor;
                break;
            case "Ravenclaw":
                resource = R.drawable.ravenclaw;
                break;
            case "Slytherin":
                resource = R.drawable.slytherin;
                break;
            case "Hufflepuff":
                resource = R.drawable.hufflepuff;
                break;
        }
        return resource;
    }
    public static void loadImage(String name, ImageView imageView)
    {
        Picasso.get().load(IMAGE_URL+name.toLowerCase()+".jpg")
                .error(R.drawable.user_icon)
                .into(imageView);
    }
}