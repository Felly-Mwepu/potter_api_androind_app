package com.felly.harrypotter.core.utils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class DataResources<T>
{
    @NonNull
    public final Status status;
    @Nullable
    public final T data;
    @Nullable
    public final String message;

    public DataResources(@NonNull Status status, @Nullable T data, @Nullable String message)
    {
        this.status = status;
        this.data = data;
        this.message = message;
    }

    public static <T> DataResources<T> loading(String message)
    {
        return new DataResources<>(Status.LOADING, null, message);
    }
    public static <T> DataResources<T> error(@Nullable T data, @Nullable String message)
    {
        return new DataResources<>(Status.ERROR, data, message);
    }
    public static <T> DataResources<T> success(@Nullable T data)
    {
        return new DataResources<>(Status.SUCCESS, data, null);
    }
    public static <T> DataResources<T> busy(@Nullable T data)
    {
        return new DataResources<>(Status.BUSY, data, null);
    }
    public enum Status
    {
        SUCCESS,
        ERROR,
        LOADING,
        BUSY
    }
}

