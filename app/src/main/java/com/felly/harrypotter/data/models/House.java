package com.felly.harrypotter.data.models;

import com.google.gson.JsonArray;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.List;

public class House implements Serializable {
    private String _id;
    private String name;
    private String mascot;
    private String headOfHouse;
    private String houseGhost;
    private String founder;
    private String school;


    public House(String _id, String name, String mascot, String headOfHouse, String houseGhost, String founder, String school) {
        this._id = _id;
        this.name = name;
        this.mascot = mascot;
        this.headOfHouse = headOfHouse;
        this.houseGhost = houseGhost;
        this.founder = founder;
        this.school = school;
    }


    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMascot() {
        return mascot;
    }

    public void setMascot(String mascot) {
        this.mascot = mascot;
    }

    public String getHeadOfHouse() {
        return headOfHouse;
    }

    public void setHeadOfHouse(String headOfHouse) {
        this.headOfHouse = headOfHouse;
    }

    public String getHouseGhost() {
        return houseGhost;
    }

    public void setHouseGhost(String houseGhost) {
        this.houseGhost = houseGhost;
    }

    public String getFounder() {
        return founder;
    }

    public void setFounder(String founder) {
        this.founder = founder;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }
//
//    public JsonArray  getMembers() {
//        return members;
//    }
//
//    public void setMembers(JsonArray members) {
//        this.members = members;
//    }

    @NotNull
    @Override
    public String toString() {
        return "House{" +
                "_id='" + _id + '\'' +
                ", name='" + name + '\'' +
                ", mascot='" + mascot + '\'' +
                ", headOfHouse='" + headOfHouse + '\'' +
                ", houseGhost='" + houseGhost + '\'' +
                ", founder='" + founder + '\'' +
                ", school='" + school + '\'' +
//                ", members=" + members +
                '}';
    }
}
