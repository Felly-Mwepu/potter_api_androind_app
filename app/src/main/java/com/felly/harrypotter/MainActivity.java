package com.felly.harrypotter;


import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.dynamicfeatures.fragment.DynamicNavHostFragment;
import androidx.navigation.ui.NavigationUI;

import com.felly.harrypotter.core.utils.Constants;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import dagger.android.support.DaggerAppCompatActivity;

public class MainActivity extends DaggerAppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener
{
    private NavController navController;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_nav);

        Fragment navHostFragment =  getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
        if(navHostFragment != null){
            navController = DynamicNavHostFragment.findNavController(navHostFragment);
            NavigationUI.setupWithNavController(bottomNavigationView, navController);
        }
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Bundle bundle = new Bundle();
        switch (item.getItemId()){
            case R.id.houses:
                navController.navigate(R.id.housesFragment);
                break;
            case R.id.students:
                bundle.putString(Constants.STUDENTS_EXTRA,Constants.STUDENTS_EXTRA );
                navController.navigate(R.id.charactersFragment, bundle);
                break;
            case R.id.characters:
                bundle.putString(Constants.CHARACTERS_EXTRA,Constants.CHARACTERS_EXTRA );
                navController.navigate(R.id.charactersFragment, bundle);
                break;
            case R.id.spells:
                navController.navigate(R.id.spellsFragment);
                break;
        }
        return true;
    }
}
