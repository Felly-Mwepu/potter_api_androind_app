package com.felly.harrypotter.di.components;

import android.app.Application;

import com.felly.harrypotter.core.MyApplication;
import com.felly.harrypotter.di.modules.ActivityBuilderModules;
import com.felly.harrypotter.di.modules.AppModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(
        modules = {
                AndroidSupportInjectionModule.class,
                AppModule.class,
                ActivityBuilderModules.class,
        }
)
public interface AppComponent extends AndroidInjector<MyApplication> {
    @Component.Builder
    interface Builder
    {
        @BindsInstance
        Builder application(Application application);
        AppComponent build();
    }

}
