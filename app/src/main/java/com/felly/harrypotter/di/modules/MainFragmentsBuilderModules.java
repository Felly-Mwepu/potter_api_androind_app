package com.felly.harrypotter.di.modules;

import com.felly.harrypotter.ui.houses.HousesFragment;
import com.felly.harrypotter.ui.characters.CharactersFragment;
import com.felly.harrypotter.ui.spell.SpellsFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainFragmentsBuilderModules
{
    @ContributesAndroidInjector(modules = {HousesFragmentModule.class,})
    abstract HousesFragment contributeHousesFragment();

    @ContributesAndroidInjector
    abstract CharactersFragment contributeCharactersFragment();

    @ContributesAndroidInjector
    abstract SpellsFragment contributeSpellsFragment();
}
