package com.felly.harrypotter.ui.houses;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import androidx.navigation.dynamicfeatures.fragment.DynamicNavHostFragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.CompositePageTransformer;
import androidx.viewpager2.widget.MarginPageTransformer;
import androidx.viewpager2.widget.ViewPager2;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.felly.harrypotter.R;
import com.felly.harrypotter.core.callbacks.ItemClickedListener;
import com.felly.harrypotter.core.utils.Constants;
import com.felly.harrypotter.data.models.House;
import com.felly.harrypotter.di.viewmodel.ViewModelProviderFactory;
import com.google.android.material.bottomnavigation.BottomNavigationView;


import javax.inject.Inject;

import dagger.android.support.DaggerFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class HousesFragment extends DaggerFragment implements ItemClickedListener<House>{

    @Inject
    ViewModelProviderFactory viewModelProvider;

    @Inject HousesAdapter adapter;

    private ViewPager2 viewPager;
    private RelativeLayout progressBarLayout;
    private ProgressBar progressBar;
    private TextView errorTextView;

    private HouseFragmentViewModel viewModel;
    public HousesFragment()
    {
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_houses, container, false);
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        selectNavBarMenuItem();
        viewModel = new ViewModelProvider(this, viewModelProvider).get(HouseFragmentViewModel.class);
        initViews(view);
        observeHousesData();
        viewModel.requestHouses();
    }
    private ViewPager2.PageTransformer pageTransformer(){
        return (page, position) -> {
            float r = 1 - Math.abs(position);
            page.setScaleX(0.85f + r * 0.15f);
        };
    }
    private void initViews(View view)
    {

        viewPager = view.findViewById(R.id.housePagerSlider);
        progressBarLayout = view.findViewById(R.id.progress_bar_layout);
        errorTextView = view.findViewById(R.id.errorText);
        progressBar = view.findViewById(R.id.progressBar);
        viewPager.setAdapter(adapter);

        viewPager.setClipToPadding(false);
        viewPager.setClipChildren(false);
        viewPager.setOffscreenPageLimit(3);
        viewPager.getChildAt(0).setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER);

        CompositePageTransformer compositePageTransformer = new CompositePageTransformer();

        compositePageTransformer.addTransformer(new MarginPageTransformer(40));
        compositePageTransformer.addTransformer(pageTransformer());
        viewPager.setPageTransformer(compositePageTransformer);
        adapter.setOnItemClickedListener(this);
    }
    private void selectNavBarMenuItem(){
        if(getActivity() != null){
            BottomNavigationView bottomNavigationView = getActivity().findViewById(R.id.bottom_nav);
            bottomNavigationView.getMenu().getItem(0).setChecked(true);
        }
    }

    private void observeHousesData(){
        viewModel.getHouses().removeObservers(getViewLifecycleOwner());
        viewModel.getHouses().observe(this, listDataResources -> {
            if (listDataResources != null)
            {
                switch (listDataResources.status)
                {
                    case LOADING:
                        showProgressBar(false, null);
                        break;
                    case ERROR:
                        showProgressBar(true, listDataResources.message);
                        break;
                    case SUCCESS:
                        if(listDataResources.data != null && listDataResources.data.size() > 0)
                        {
                            showProgressBar(true, "success");
                            adapter.setHouseList(listDataResources.data);
                        }
                        else
                            showProgressBar(true, "No data available");
                        break;
                }
            }
            else
                showProgressBar(true, "Failed to connect, Try again");

        });
    }
    private void showProgressBar(boolean isShowing, String message){

        if(isShowing){

            if(message.equals("success"))
            {
                progressBarLayout.setVisibility(View.GONE);
                errorTextView.setVisibility(View.GONE);
                viewPager.setVisibility(View.VISIBLE);
            }
            else
            {
                viewPager.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                errorTextView.setText(message);
                errorTextView.setVisibility(View.VISIBLE);
            }
        }
        else{
            viewPager.setVisibility(View.GONE);
            errorTextView.setVisibility(View.GONE);
            progressBarLayout.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);

        }
    }
    @Override
    public void onItemClicked(House house) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.HOUSE_EXTRA, house);
        DynamicNavHostFragment.findNavController(this).navigate(R.id.charactersFragment, bundle);
    }

}
