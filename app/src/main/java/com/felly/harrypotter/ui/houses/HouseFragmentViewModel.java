package com.felly.harrypotter.ui.houses;




import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;

import com.felly.harrypotter.core.utils.Constants;
import com.felly.harrypotter.core.utils.DataResources;
import com.felly.harrypotter.data.api.PotterApi;
import com.felly.harrypotter.data.models.House;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
public class HouseFragmentViewModel extends ViewModel
{
    private PotterApi api;
    private MediatorLiveData<DataResources<List<House>>> resources = new MediatorLiveData<>();
    @Inject
    public HouseFragmentViewModel(PotterApi api){
        this.api = api;
    }
    void requestHouses(){
        resources.setValue(DataResources.loading("Loading, Please Wait..."));
        final LiveData<DataResources<List<House>>> source = LiveDataReactiveStreams.fromPublisher(
        api.getHouses(Constants.API_KEY)
                .onErrorReturn(throwable -> Collections.singletonList(new House("error","","","","","","")))
                .map((Function<List<House>, DataResources<List<House>>>) houses -> {
                if(houses.get(0).get_id().equals(Constants.ERROR))
                    return DataResources.error(null,"Failed to connect. Please try again");
                return DataResources.success(houses);
            }).subscribeOn(Schedulers.io())
        );
        //Removing MediatorLiveData Source
        resources.addSource(source, listDataResources -> {
            resources.setValue(listDataResources);
            resources.removeSource(source);
        });
    }

    public LiveData<DataResources<List<House>>> getHouses(){
        return resources;
    }
}
