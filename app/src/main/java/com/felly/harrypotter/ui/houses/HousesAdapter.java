package com.felly.harrypotter.ui.houses;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.felly.harrypotter.R;
import com.felly.harrypotter.core.callbacks.ItemClickedListener;
import com.felly.harrypotter.core.utils.Constants;
import com.felly.harrypotter.data.models.House;
import java.util.ArrayList;
import java.util.List;

public class HousesAdapter extends RecyclerView.Adapter<HousesAdapter.HouseSliderViewHolder>
{
    private List<House> houseList = new ArrayList<>();
    private ItemClickedListener<House> listener;

    public void setHouseList(List<House> houseList){
        if(this.houseList.size() > 0)
            this.houseList.clear();

        this.houseList.addAll(houseList);
        notifyDataSetChanged();
    }
    public void setOnItemClickedListener(ItemClickedListener<House> listener){
        this.listener = listener;
    }
    @NonNull
    @Override
    public HouseSliderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new HouseSliderViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(
                            R.layout.house_item, parent, false)
        );
    }
    @Override
    public void onBindViewHolder(@NonNull HouseSliderViewHolder holder, int position) {
        holder.bind(houseList.get(position));
    }
    @Override
    public int getItemCount() {
        return houseList.size();
    }

    class HouseSliderViewHolder extends RecyclerView.ViewHolder
    {
        private ImageView houseIcon;
        private TextView houseNameView;

        HouseSliderViewHolder(@NonNull View itemView) {
            super(itemView);
            houseIcon = itemView.findViewById(R.id.house_icon);
            houseNameView = itemView.findViewById(R.id.house_name);
        }
        private void bind(House house){
            setHouseIcon(house.getName());
            itemView.setOnClickListener(view -> listener.onItemClicked(house));
        }
        private void setHouseIcon(String houseName){
            houseIcon.setImageResource(Constants.getHouseIncon(houseName));
            houseNameView.setText(houseName);
        }
    }
}
