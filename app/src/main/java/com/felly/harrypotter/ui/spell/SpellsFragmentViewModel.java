package com.felly.harrypotter.ui.spell;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;

import com.felly.harrypotter.core.utils.Constants;
import com.felly.harrypotter.core.utils.DataResources;
import com.felly.harrypotter.data.api.PotterApi;

import com.felly.harrypotter.data.models.Spell;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class SpellsFragmentViewModel extends ViewModel {
    private static final String TAG = "SpellsFragmentViewModel";
    private PotterApi api;
    private MediatorLiveData<DataResources<List<Spell>>> resources = new MediatorLiveData<>();
    @Inject
    public SpellsFragmentViewModel(PotterApi api)
    {
        this.api = api;
    }
    void requestSpells(){
        resources.setValue(DataResources.loading("Loading, Please Wait..."));
        final LiveData<DataResources<List<Spell>>> source = LiveDataReactiveStreams.fromPublisher(
                api.getSpells(Constants.API_KEY)
                        .onErrorReturn(throwable ->{
                            Log.d(TAG, "requestSpells: Error Message: "+throwable.getMessage());
                            return Collections.singletonList(new Spell("error",null,null,null));
                        })
                        .map((Function<List<Spell>, DataResources<List<Spell>>>) houses -> {
                            if(houses.get(0).get_id().equals(Constants.ERROR))
                                return DataResources.error(null,"Failed to connect. Please try again");
                            return DataResources.success(houses);
                        }).subscribeOn(Schedulers.io())
        );
        //Removing MediatorLiveData Source
        resources.addSource(source, listDataResources -> {
            resources.setValue(listDataResources);
            resources.removeSource(source);
        });
    }
    LiveData<DataResources<List<Spell>>> getAllSpells(){
        return resources;
    }
}
