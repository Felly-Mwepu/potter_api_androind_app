Base URL = https://www.potterapi.com/v1/
API Key = $2a$10$1JEnmtEF417yBaFZcr51qukRjaKv8d5toEG5DKP/IUZWIVwfsaF7y

characters URL = https://www.potterapi.com/v1/characters?key=$2a$10$1JEnmtEF417yBaFZcr51qukRjaKv8d5toEG5DKP/IUZWIVwfsaF7y

/*********************/
Images Api:

Base URL = http://hp-api.herokuapp.com

e.g : "http://hp-api.herokuapp.com/images/hermione.jpeg"

>>> Get the list of characters and convert the first name to lowercase to load the image.